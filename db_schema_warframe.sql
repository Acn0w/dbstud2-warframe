/* Author: Alessandro Cecchin AKA Acn0w */
CREATE TABLE Users
(
    email VARCHAR(256) NOT NULL PRIMARY KEY,    --for now 256 character seems a good deal
    password VARCHAR(100) NOT NULL,
    platform VARCHAR(4) NOT NULL,               --can be chosen from "PS4" or "PC" or "Xbox"
    user_banned BOOLEAN DEFAULT	FALSE,
    operator_nick VARCHAR(15) NOT NULL,

    FOREIGN KEY (operator_nick) REFERENCES Operator(nick)  --foreign key to the table Operator
        ON DELETE CASCADE ON UPDATE CASCADE,


    CONSTRAINT email_domain
        CHECK(email LIKE '%@%'),                 --weak check on email

    CONSTRAINT platform_domain                   --just choose from 3 type otherwise not valid
        CHECK(platform IN ('PS4','Xbox','PC'))      --IN indicates that platform must be in the speciefied set
);

CREATE TABLE Operator
(
    nick VARCHAR(15) PRIMARY KEY NOT NULL,
    mastery_rank SMALLINT,                         --level of the operator
    restricted_quest BOOLEAN DEFAULT false,       --need a specific quest to use the operator


    CONSTRAINT mastery_rank_domain                       --mr must be between 0 and 30
        CHECK(mastery_rank>=0 AND mastery_rank<=30)

);

CREATE TABLE Wear
(
    operator_nick VARCHAR(15) NOT NULL,
    warframe_name VARCHAR(20) NOT NULL,

    PRIMARY KEY (operator_nick,warframe_name),
    FOREIGN KEY (operator_nick) REFERENCES Operator(nick)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (warframe_name) REFERENCES Warframe(name)
        ON DELETE RESTRICT ON UPDATE CASCADE

);

CREATE TABLE Warframe
(
    name VARCHAR(20) NOT NULL PRIMARY KEY,
    abil1 VARCHAR(256) UNIQUE NOT NULL,              --a warframe must have 4 unique abilities
    abil2 VARCHAR(256) UNIQUE NOT NULL,
    abil3 VARCHAR(256) UNIQUE NOT NULL,
    abil4 VARCHAR(256) UNIQUE NOT NULL,
    passive_abil VARCHAR(256) UNIQUE NOT NULL       --every warframe have a unique passive ability

);


CREATE TABLE PoweredWith
(
    warframe_name VARCHAR(20),
    mods_name VARCHAR(20),

    PRIMARY KEY (warframe_name,mods_name),
    FOREIGN KEY (warframe_name) REFERENCES Warframe(name)  --they are somehow indipendent to each other
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (mods_name) REFERENCES Mods(name)
        ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE Mods
(
    name VARCHAR(20) NOT NULL PRIMARY KEY,
    polarity CHAR,                          --it can be M,V,N,Z,P,U,E(mpty)
    rarity CHAR NOT NULL,                  --it can be C,U,R,L
    mod_desc VARCHAR(100),

    CONSTRAINT polarity_domain
        CHECK (polarity IN ('M','V','N','Z','P','U','E')),
    CONSTRAINT rarity_domain
        CHECK (rarity IN ('C','U','R','L'))

);

CREATE TABLE Equip
(
    weapons_name VARCHAR(30) NOT NULL,
    operator_nick VARCHAR(20) NOT NULL,

    PRIMARY KEY (weapons_name,operator_nick),
    FOREIGN KEY (weapons_name) REFERENCES Weapons(name)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (operator_nick) REFERENCES Operator(nick)
        ON DELETE RESTRICT ON UPDATE CASCADE

);

CREATE TABLE Weapons
(
    name VARCHAR(30) NOT NULL PRIMARY KEY,
    damage SMALLINT,                        --total damage of a weapon
    type CHAR NOT NULL,              --it can be P(rimary) or S(econdary) or M(elee).
    min_mastery_rank SMALLINT,              --some weapon can be used only with a minimun
                                                --masteryrank.

    CONSTRAINT weapon_type_domain
        CHECK (type IN ('P','S','M')),
    CONSTRAINT weapon_min_mastery_rank_domain
        CHECK (min_mastery_rank<=30 AND min_mastery_rank>=0)
);

CREATE TABLE UpgradedWith
(
    weapons_name VARCHAR(30),
    mods_name VARCHAR(20),

    PRIMARY KEY (weapons_name,mods_name),
    FOREIGN KEY (weapons_name) REFERENCES Weapons(name)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (mods_name) REFERENCES Mods(name)
        ON DELETE RESTRICT ON UPDATE CASCADE

);

CREATE TABLE HelpedBy
(
    companion_name VARCHAR(20) NOT NULL,
    operator_nick VARCHAR(20) NOT NULL,

    PRIMARY KEY (companion_name,operator_nick),
    FOREIGN KEY (companion_name) REFERENCES Companion(name)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (operator_nick) REFERENCES Operator(nick)
        ON DELETE RESTRICT ON UPDATE CASCADE

);

CREATE TABLE Companion
(
    name VARCHAR(20) PRIMARY KEY NOT NULL,
    ability VARCHAR(20) NOT NULL,
    damage SMALLINT,
    type CHAR(2) NOT NULL,                     --can be KU(brow), KA(vat),SE(ntinel)

    CONSTRAINT companion_type_domain
        CHECK (type IN ('KU','KA','SE'))

);

CREATE TABLE Installed
(
    companion_name VARCHAR(20),
    mods_name VARCHAR(20),

    PRIMARY KEY (companion_name, mods_name),
    FOREIGN KEY (companion_name) REFERENCES Companion(name)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (mods_name) REFERENCES Mods(name)
        ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE CanHave
(
    weapons_name VARCHAR(30) NOT NULL,
    companion_name VARCHAR(20) NOT NULL,

    PRIMARY KEY (weapons_name,companion_name),
    FOREIGN KEY (weapons_name) REFERENCES Weapons(name)
        ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (companion_name) REFERENCES Companion(name)
        ON DELETE RESTRICT ON UPDATE CASCADE
);
