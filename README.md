**SIMPLE DATABASE FOR WARFRAME**<br>
This is just a homework for my exam of Foundations of Database.
Digital Extreme didn't commission me anything.<br>
The Entity–Relationship Model can be found in the Report near the end of the file.
<br>
<br>
This project is a semplified version of the database used for Warframe: it has just a <br>
small set of entities and less constraints on the relations between them. <br>
The model is implemented in PostgreSQL and all the contents of tables are writed<br
in txt files.
